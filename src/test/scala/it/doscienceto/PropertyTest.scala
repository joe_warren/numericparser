package it.doscienceto

import org.scalatest.prop.PropertyChecks
import org.scalatest.{PropSpec, Matchers}

class PropertyTest extends PropSpec with Matchers with PropertyChecks {
  property("parse should invert write" ) {
    forAll { (v: Int) =>

      whenever (v > 0 && v != Integer.MAX_VALUE) {
        NumericParse(NumericWrite(v)) should be (v)
      }
    }
  }

}
