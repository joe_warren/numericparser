package it.doscienceto

import org.scalatest.{FlatSpec, Matchers}

class NumericWriteSpec extends FlatSpec with Matchers{
  "NumericWrite" should "convert simple units to their text value" in {
    for(v <-singlets) {
      NumericWrite(v._2) should be(v._1)
    }
    for(v <-tenlets) {
      NumericWrite(v._2) should be(v._1)
    }
  }
  it should "write two digit numbers" in {
    NumericWrite(22) should be("twenty two")
    NumericWrite(34) should be ("thirty four")
    NumericWrite(59) should be ("fifty nine")
  }
  it should "write numbers in the hundreds" in {
    NumericWrite(500) should be("five hundred")
    NumericWrite(100) should be ("one hundred")
    NumericWrite(203) should be ("two hundred and three")
    NumericWrite(645) should be ("six hundred and forty five")
  }
  it should "write numbers in the thousands" in {
    NumericWrite(5000) should be("five thousand")
    NumericWrite(100000) should be("one hundred thousand")
    NumericWrite(200100) should be ("two hundred thousand one hundred")
    NumericWrite(1045) should be ("one thousand and forty five")
  }
  it should "write numbers in the millions" in {
    NumericWrite(5000000) should be("five million")
    NumericWrite(100000000) should be ("one hundred million")
    NumericWrite(2000000000) should be ("two thousand million")
    NumericWrite(1200545) should be ("one million two hundred thousand five hundred and forty five")
  }
}
