package it.doscienceto

import org.scalatest.{Matchers, FlatSpec}

class NumericParseSpec extends FlatSpec with Matchers{

  "NumericParse" should "convert simple units into their value" in {
    for(v <-singlets) {
      NumericParse(v._1) should be(v._2)
    }
    for(v <-tenlets) {
      NumericParse(v._1) should be(v._2)
    }
  }
  it should "parse two digit numbers" in {
    NumericParse("twentytwo") should be(22)
    NumericParse("thirtyfour") should be (34)
    NumericParse("fifty nine") should be (59)
  }
  it should "parse numbers in the hundreds" in {
    NumericParse("five hundred") should be(500)
    NumericParse("one hundred") should be (100)
    NumericParse("two hundred and three") should be (203)
    NumericParse("six hundred forty five") should be (645)
  }
  it should "parse numbers in the thousands" in {
    NumericParse("five thousand") should be(5000)
    NumericParse("one hundred thousand") should be (100000)
    NumericParse("two hundred thousand one hundred") should be (200100)
    NumericParse("one thousand and forty five") should be (1045)
  }
  it should "parse numbers in the millions" in {
    NumericParse("five million") should be(5000000)
    NumericParse("one hundred million") should be (100000000)
    NumericParse("two thousand million") should be (2000000000)
    NumericParse("one million two hundred thousand five hundred and forty five") should be (1200545)
  }
}
