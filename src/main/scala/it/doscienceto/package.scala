package it

/**
  * Created by joseph on 12/06/17.
  */
package object doscienceto {
  val singlets = Array(
    ("one",1),
    ("two",2),
    ("three",3),
   ("four",4),
   ("five",5),
   ("six",6),
   ("seven",7),
   ("eight",8),
   ("nine",9),
   ("ten",10),
   ("eleven",11),
   ("twelve",12),
   ("thirteen",13),
   ("fourteen",14),
   ("fifteen",15),
   ("sixteen",16),
   ("seventeen",17),
   ("eighteen",18),
   ("nineteen",19)
  )
  val singletMap = singlets.toMap
  val singletsRevMap = singlets.map(_.swap).toMap

  val tenlets = Array(
   ("twenty" ,20),
   ("thirty" ,30),
   ("forty" ,40),
   ("fifty",50),
   ("sixty",60),
   ("seventy",70),
   ("eighty",80),
   ("ninety",90)
  )

  val tenletMap = tenlets.toMap;
  val tenletRevMap = tenlets.map(_.swap).toMap
}
