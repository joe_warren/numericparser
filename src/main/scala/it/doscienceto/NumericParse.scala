package it.doscienceto

import java.text.ParseException

import scala.util.parsing.combinator._

object NumericParse extends JavaTokenParsers {
   def units: Parser[Int] = (singlets.reverse.map{case(s, i)=>Parser[String](s)}.
     reduce[Parser[String]]((_|_)) ) ^^
     {case unit => singletMap(unit)}


  def tens: Parser[Int] = (tenlets.reverse.map{case(s, i)=>Parser[String](s)}.
    reduce[Parser[String]]((_|_)) ) ^^
    {case unit => tenletMap(unit)}

  def twoDigitExact = (tens ~ units) ^^ {case a~b => a+b}
  def twoDigit = twoDigitExact|tens|units

  def hundreds: Parser[Int] = (twoDigit ~ "hundred") ^^ {case h~_ => h*100}

  def and = "and"|"";

  def threeDigitExact: Parser[Int] = (hundreds ~ and ~ twoDigit)^^{case h~_~r => h+r}
  def threeDigit = threeDigitExact | hundreds | twoDigit

  def thousands = (threeDigit ~ "thousand") ^^ {case t~_ => t*1000}

  def fiveDigitExact = (thousands ~ and ~ threeDigit)^^{case t~_~r => t+r}
  def fiveDigit = fiveDigitExact | thousands | threeDigit

  def millions = (fiveDigit ~ "million") ^^ {case m~_=> m*1000000}

  def nineDigitExact = (millions ~ and ~ fiveDigit)^^{case m~_~r=>m+r}
  def nineDigit = nineDigitExact | millions | fiveDigit

  def number: Parser[Int] = nineDigit

  def apply(input: String): Int = parseAll(number, input) match {
    case Success(result, _) => result
    case failure: NoSuccess => {
      throw new ParseException(failure.msg, failure.next.pos.column)
    }
  }
}
