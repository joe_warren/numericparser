package it.doscienceto

object NumericWrite {
  val million = 1000000
  val thousand = 1000
  val hundred = 100

  def addAnd(s :String): String = {
    if(s != "") {
      return "and " + s
    }
    return ""
  }
  def addSpace(s :String): String = {
    if(s != "") {
      return " " + s
    }
    return ""
  }

  def write(v:Int, nested:Boolean=false) :String = {
    if(v>=million){
      return write(v/million)+ " million" + addSpace(write(v%million, true))
    }
    else if(v>=thousand){
      return write(v/thousand)+ " thousand" + addSpace(write(v%thousand, true))
    }
    if(v>=hundred){
      return write(v/hundred)+ " hundred" + addSpace(write(v%hundred, true))
    }
    if(singletsRevMap.contains(v)){
      val r = singletsRevMap(v)
      if( nested ){
        return addAnd(r)
      } else {
        return r
      }
    }
    if(tenletRevMap.contains(v)){
      val r = tenletRevMap(v)
      if( nested ){
        return addAnd(r)
      } else {
        return r
      }
    }
    if(v==0){
      return ""
    }
    val r = write(v-v%10) +" "+ write(v%10)
    if( nested ){
      return addAnd(r)
    } else {
      return r
    }
  }

  def apply(v:Int) :String = write(v, false)
}
