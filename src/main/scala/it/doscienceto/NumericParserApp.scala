package it.doscienceto

object NumericParserApp {
  def main(argv: Array[String]): Unit = {
    argv.toList match {
      case "-i" :: ints => ints.map(_.toInt).map(NumericWrite(_)).foreach(println)
      case parts => println(NumericParse(parts.mkString(" ")).toString)
    }
  }
}
